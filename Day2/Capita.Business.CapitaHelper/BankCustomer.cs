﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capita.Business.CapitaHelper
{
    public class BankCustomer
    {
        public decimal Balance { get; set; }

        public void Credit(decimal amount )
        {
           Balance += amount;
        }
        public void Debit(decimal amount)
        {
            if (Balance > amount)
            {
                Balance -= amount;
            }
            else
            {
                throw new InsufficientBalanceException("অপর্যাপ্ত অর্থ   Aparyāpta artha ");
            }

        }
    }
}
