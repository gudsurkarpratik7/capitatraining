﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capita.Business.CapitaHelper
{
    public interface IAnimals
    {
        void Breathe();
    }
    public abstract class Mammals:IAnimals
    {
        public virtual void Breathe()
        {
            Console.WriteLine("mammal breathing");
        }

        public abstract void Eating();

       
    }

    public interface IHobby
    {
        void Dancing();
        void Breathe();
    }
    public interface IMeditation
    {
        void Breathe();
    }
    public class Human : Mammals, IHobby,IMeditation
    {
        public override void Breathe()
        {
            Console.WriteLine("Human breath");
        }
         void IHobby.Breathe()
        {
            Console.WriteLine("Hobby wala  breath");
        }
        void IMeditation.Breathe()
        {
            Console.WriteLine("Meditation wala  breath");
        }
        public void Dancing()
        {
            Console.WriteLine("Hobby wala Dancing");
        }

        public override void Eating()
        {
            Console.WriteLine("Human Eating");
        }
    }

   
}
