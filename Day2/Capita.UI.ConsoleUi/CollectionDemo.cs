﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
    namespace Capita.UI.ConsoleUi
{
    class CollectionDemo
    {
        public static void Main()
        {
            ArrayList list1 = new ArrayList();
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(3.5f);
            list1.Add(5);
            list1.Add(28);
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(3.5f);
            list1.Add(3.5f);
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(3.5f);
            list1.Add(5);
            list1.Add(28);
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(3.5f);
            list1.Add(5);
            list1.Add(28);
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(3.5f);
            list1.Add(3.5f);
            list1.Add(1);
            list1.Add("Hello");
            list1.Add(3.5f);
            list1.Add(5);
            list1.Add(28);
            list1.Add(1);
            list1.Add("Hello");
            list1.Add("Hello");
            foreach (dynamic item1 in list1)
            {
                Console.WriteLine(item1);
            }
            Console.WriteLine(list1.Count);
            Console.WriteLine(list1.Capacity);
            Console.ReadLine(  );
        }
    }
}
