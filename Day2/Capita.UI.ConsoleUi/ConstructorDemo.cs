﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capita.UI.ConsoleUi
{
     class ParentClass
    {
        public ParentClass()
        {
            Console.WriteLine("Parent Class default constructor");
        }
        public ParentClass(String str)
        {
            Console.WriteLine("Parent Class parameterized constructor "+str);
        }
        ~ParentClass()
        {
            Console.WriteLine("Parent class destructor");
            Console.ReadLine();
        }
        public static void MyMethod()
        {
            Console.WriteLine("This is MyMethod");
        }
    }
    class ChildClass:ParentClass
    {
        public static int myCopy = 0;
        public ChildClass()
        {
            myCopy++;
            Console.WriteLine("Child default constructor");
        }
        public ChildClass(String str)
        {
            myCopy++;
            Console.WriteLine("Child Class parameterized constructor " + str);
        }
        ~ChildClass()
        {
            Console.WriteLine("Child class destructor");
        }
    }
    class ConstructorDemo
    {
        public static void Main()
        {
            ChildClass C = new ChildClass("Pizza");
            ChildClass C1 = new ChildClass("Dal");
            ChildClass.MyMethod();
            ParentClass.MyMethod();
            Console.WriteLine("myCopy already exists of"+ChildClass.myCopy);
            Console.ReadLine();
        }
    }
}
