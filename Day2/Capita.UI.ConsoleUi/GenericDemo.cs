﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capita.UI.ConsoleUi
{
    class Alter<T>// where T:class
    {
        public void Swap(ref T num1,ref T  num2)
        {
            T temp = num1;
            num1 = num2;
            num2 = temp;
        }
    }
    class GenericDemo
    {
        public static void Main()
        {
            int num1 = 5;
            int num2 = 3;
            string str1 = "Pratik";
            string str2 = "Shubhra";
            Alter<int> alt = new Alter<int>();
            Console.WriteLine("Before Swapping Number1: " + num1 + "Number2:  " + num2);
            alt.Swap(ref num1, ref num2);
            Console.WriteLine("After Swapping Number1: " + num1 + "Number2:  " + num2);

            Alter<string> alt1 = new Alter<string>();
            Console.WriteLine("Before Swapping Number1: " + str1 + "Number2:  " + str2);
            alt1.Swap(ref str1, ref str2);
            Console.WriteLine("After Swapping Number1: " + str1+ "Number2:  " + str2);
            Console.ReadLine();
        }
    }
}
