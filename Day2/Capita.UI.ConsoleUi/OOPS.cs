﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capita.UI.ConsoleUi
{
    //abstract class Shape
    //{
    //    public double area;
    //    public abstract double FindArea();
    //}
    class Shape
    {
        public double area;
        public virtual double FindArea()
        {
            return 0;
        }
    }
    class Circle :Shape
    {

        public double Radius
        {
            get;
            set;
        }

        public override double FindArea()
        {
            return 3.14 * Radius * Radius; 
        }
    }
    class OOPS
    {
        public static void Main()
        {
            Shape s;
            Circle c1 = new Circle();
            c1.area = 10;
            c1.Radius = 3;
            //Console.WriteLine("The area is: " + c1.area.ToString());
            //Console.WriteLine("The area is: " + c1.FindArea());
            s = c1;
            Console.WriteLine("The area is: " + s.area.ToString());
            Console.WriteLine("The area is: " + s.FindArea());
            Console.ReadLine();
        }
    }
}
